import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'

import index from '@/components/index';
// import helloWorld from '@/components/HelloWorld';

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: 'vue-build',
  routes: [
    {
      path: '/vue-cv',
      alias: '/vue-cv.html',
      name: 'index',
      component: index
    },
    // {
    //   path: '/contact',
    //   name: 'contact',
    //   component: contact
    // },
    // {
    //   path: '/about',
    //   name: 'about',
    //   component: about
    // },
    // {
    //   path: '/portfolio',
    //   name: 'portfolio',
    //   component: portfolio
    // },
    // {
    //   path: '/resume',
    //   name: 'resume',
    //   component: resume
    // },
    // {
    //   path: '/testimonials',
    //   name: 'testimonials',
    //   component: testimonials
    // },
    // {
    //   path: '/helloWorld',
    //   name: 'helloWorld',
    //   component: helloWorld
    // }
  ]
})
